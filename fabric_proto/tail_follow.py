#!/usr/bin/env python3

'''
Fabric Testing - Tail -f Demo
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"


import time
from fabric import Connection
from invoke import Responder


CENT8_VM_IP = '172.16.26.128'
CENT8_VM_UN = 'scking'
CENT8_VM_PW = 'scking'


def main():
    '''
    - Create a persistent connection
    - Run a number of commands
    - End connection
    '''
    with Connection(CENT8_VM_IP, CENT8_VM_UN, connect_timeout=60, connect_kwargs={"password": CENT8_VM_PW}) as cnx:
        sudopass = Responder(pattern=r'\[sudo\] password for scking:', response='scking\n')
        print('INFO: Follow /var/log/messages via [sudo tail -n 5 -f /var/log/messages]')
        cnx.run('sudo tail -n 5 -f /var/log/messages', pty=True, watchers=[sudopass])

    cnx.close()


if __name__ == "__main__":
    main()
