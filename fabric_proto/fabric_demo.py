#!/usr/bin/env python3

'''
Fabric Testing
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"


import time
from fabric import Connection
from invoke import Responder
from invoke import Promise


CENT8_VM_IP = '172.16.26.128'
CENT8_VM_UN = 'scking'
CENT8_VM_PW = 'scking'


def delim(character='-', length=60, title=''):
    ''' Add a delimiter of chars, length, with optional title. '''
    head = '{:%s^%ss}' % (character, length)
    header = head.format(title)
    print(header)


def main():
    '''
    - Create a persistent connection
    - Run a number of commands
    - End connection
    '''
    with Connection(CENT8_VM_IP, CENT8_VM_UN, connect_kwargs={"password": CENT8_VM_PW}) as cnx:
        sudopass = Responder(pattern=r'\[sudo\] password for scking:', response='scking\n')
        delim(title=' System Info ')
        cnx.run('uname -a')
        cnx.run('cat /etc/os-release')
        delim(title=' Networking ')
        cnx.run('ip addr')
        delim(title=' Storage ')
        print('\nThis needs sudo. Will fail.')
        cnx.run('fdisk -l')
        print('\nManually respond to sudo prompt...')
        cnx.run('sudo fdisk -l', pty=True)
        print('\n\nUse Responder to do this for you...')
        cnx.run('sudo fdisk -l', pty=True, watchers=[sudopass])
        result = cnx.run('blkid')
        print('Exit Code for blkid command:', result.return_code)
        print(result.stdout)
        delim(title=' Synchronous ')
        cnx.run('echo "ABC running for 3 seconds." ; sleep 3 ; echo "ABC complete."')
        cnx.run('echo "DEF running for 2 seconds." ; sleep 2 ; echo "DEF complete."')
        delim(title=f' Asynchronous: Start ')
        start = time.time()
        print('ABC running for 10s')
        promise1 = cnx.run('echo "ABC starting (10s)." ; sleep 10 ; echo "ABC complete."', asynchronous=True)
        print('DEF running for 5s.')
        promise2 = cnx.run('echo "DEF starting (5s)." ; sleep 5 ; echo "DEF complete."', asynchronous=True)
        print('\nWhen asynchronous=True, these become Promise instances.\nJoining blocks until both complete.')
        print('\nWhen the joined Promise instances complete, they become\nResult instances, with .stdout and'
              ' .exited attributes.\n')
        result1 = promise1.join()
        result2 = promise2.join()
        print (f'INFO: Duration - [{round((time.time() - start), 2)}s]')
        delim(title=f' Asynchronous: End ')
        print()
        delim(title=f' Promise 1 Output (Exit Code: {result1.exited})')
        print(result1.stdout)
        delim(title=f' Promise 2 Output (Exit Code: {result2.exited})')
        print(result2.stdout)
        
        delim(title=' Disown ')
        print('FOO running for 10s, BAR running for 5s. Disowned.')
        cnx.run('echo "FOO starting (10s)." ; sleep 10 ; echo "FOO complete."', disown=True)
        cnx.run('echo "BAR starting (5s)." ; sleep 5 ; echo "BAR complete."', disown=True)


if __name__ == "__main__":
    main()
