#!/usr/bin/env python3

'''
Fabric Testing
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"


import time
from fabric import Connection
# from invoke import Responder


CENT8_VM_IP = '172.16.26.128'
CENT8_VM_UN = 'scking'
CENT8_VM_PW = 'scking'


def main():
    '''
    - Create a persistent connection
    - Run a number of commands
    - End connection
    '''
    with Connection(CENT8_VM_IP, CENT8_VM_UN, connect_kwargs={"password": CENT8_VM_PW}) as cnx:
        # sudopass = Responder(pattern=r'\[sudo\] password for scking:', response='scking\n')
        print('INFO: Connection 1 --- Background dd: [dd if=/dev/random of=/dev/null] and tee to log file...')
        cnx.run('dd if=/dev/random of=/dev/null 2>&1 | tee /tmp/log.txt', asynchronous=True)

        # Let's start a second connection to the system...
        with Connection(CENT8_VM_IP, CENT8_VM_UN, connect_kwargs={"password": CENT8_VM_PW}) as cnx2:
            print('INFO: Connection 2 --- Grab the pid for the dd process...')
            # Let's get the pid of dd...
            pid = cnx2.run("pgrep -l '^dd$' | awk {' print $1 '}").stdout

            print('INFO: Connection 2 --- kill -USR1 <pid> every 5 seconds...')
            count = 0
            while count < 3:
                cmd = f'kill -USR1 {pid}'
                cnx2.run(cmd)
                time.sleep(5)
                count += 1
                print('INFO: Connection 2 --- tail the log for dd from Connection 1...')
                cnx2.run('tail -n 3 /tmp/log.txt')
            print('INFO: Close Connection 2...')
            cnx2.close()

        print('INFO: Kill dd and close Connection 1...')
        cmd = f'kill {pid}'
        cnx.run(cmd)
        cnx.close()


if __name__ == "__main__":
    main()
