# Docker Logs
DOCKER_PS = 'docker ps -a --no-trunc -s'
DOCKER_SERVICE = 'journalctl -u docker.service  -n 10000 --no-tail'
DOCKER_CONTAINER_LOGS = "for name in `docker ps -a --format '{{{{.Names}}}}'`; do docker logs -t --tail 10000 $name > \"{output_dir}/dockerLogs/$name-10000.log\" 2> \"{output_dir}/dockerLogs/$name-10000.err\"; done"
DOCKER_CONTAINER_INSPECTS = "for name in `docker ps -a --format '{{{{.Names}}}}'`; do docker inspect $name > \"{output_dir}/dockerInspects/$name-inspect.log\" 2>&1; done"

# System Logs
SYSTEM_JOURNALCTL = 'journalctl -n 10000 --no-tail'
SYSTEM_DMESG = 'tail -n 10000 /var/log/dmesg'
SYSTEM_MESSAGES = "cp $(ls -l /var/log/messages* | head -n 5 | awk '{{print $NF}}') {output_dir}"
SYSTEM_IPTABLES = 'tail -n 10000 /var/log/iptables.log'
SYSTEM_SA_LOG = 'tail -n 10000 /var/log/sa'

# System Information
INSTALLED_RPMS = 'rpm -qa'
IP_ADDR = 'ip addr'
IP_ROUTE = 'ip route'
IP_NEIGH = 'ip neigh'
IP_NETCONF = 'ip netconf'
IP_LINK = 'ip link'
IPTABLES_SAVE = 'iptables-save'
SYSTEM_SERIAL_NUMBER = 'dmidecode -s system-serial-number'
DMIDECODE = 'dmidecode'
UPTIME = 'uptime'
DATE = 'date'
TOP = 'top -b -c -n 1'
DISK_SPACE = 'df -h'
HOSTNAME = 'hostname'
SUDOERS = 'cp /etc/sudoers {output_dir}/sudoers.txt'
FREE_MEMORY = 'free -mhltw'
BLOCK_DEVICES = 'lsblk'
ULIMIT = 'ulimit -a'
RUNNING_THREAD_COUNT = 'ps -eo nlwp | tail -n +2 | awk \'{{ num_threads += $1 }} END {{ print num_threads }}\''
PROCESS_TREE = 'ps -axjf'
EXTFRAG_INDEX = 'cat /sys/kernel/debug/extfrag/extfrag_index'
DOCKER_CGROUP = 'find /sys/fs/cgroup/*/docker -mindepth 1 -maxdepth 1 -type d -print'
CRONTAB = 'crontab -l'

# PROC INFO
PROC_PID_MAX = 'cat /proc/sys/kernel/pid_max'
PROC_BUDDY_INFO = 'cat /proc/buddyinfo'
PROC_INTERRUPTS = 'cat /proc/interrupts'
PROC_MEM_INFO = 'cat /proc/meminfo'
PROC_PAGE_TYPE_INFO = 'cat /proc/pagetypeinfo'
PROC_SLAB_INFO = 'cat /proc/slabinfo'
PROC_SWAPS = 'cat /proc/swaps'
PROC_SOFTIRQS = 'cat /proc/softirqs'
PROC_VM_ALLOC_INFO = 'cat /proc/vmallocinfo'
PROC_VMSTAT = 'cat /proc/vmstat'
PROC_ZONEINFO = 'cat /proc/zoneinfo'
PROC_VERSION = 'cat /proc/version'
IFACE_STATS = 'cat /proc/net/dev'

# Midpoint Information
MP_JOURNALCTL = 'journalctl -u midpoint-manager.service  -n 10000 --no-tail'
MP_LOG = 'tail -n 10000 /var/log/midpoint-manager.log'
MP_APP_CONFIG = 'cp {midpoint_manager_path}/appConfig.yml {output_dir}/mms-appConfig.yml'
MP_RPM = 'rpm -qi midpoint'
MP_SNORT_STATS = "for i in $(docker ps |grep snort | awk '{{print $NF'}}); do docker exec $i bash -c 'kill -usr1 `pgrep snort`'; done"
MP_TS_LOGS = """
for volume_path in `ls -ld /var/lib/docker/volumes/*traffic-summary-logs | awk '{{print $NF}}'`; do
  volume_name=${{volume_path##*/}};
  mkdir -p {output_dir}/$volume_name;
  for file_name in `ls -l $volume_path/_data/ | awk '{{print $NF}}' | grep '(log$)|(err$)' -E`; do
    tail -n 10000 $volume_path/_data/$file_name > "{output_dir}/$volume_name/10000-lines-$file_name";
  done;
done
""".replace('\n', ' ')