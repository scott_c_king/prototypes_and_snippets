#!/usr/bin/env python3

'''
Fabric Testing
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"


import subprocess
import time
from fabric import Connection
from invoke import Responder


MP_IP = '10.0.49.232'
MP_UN = 'noc'
MP_PW = 'Dr@gosU$erP@ssw0rd'


def delim(character='-', length=60, title=''):
    ''' Add a delimiter of chars, length, with optional title. '''
    head = '{:%s^%ss}' % (character, length)
    header = head.format(title)
    print(header)


def main():
    '''
    - Create a persistent connection
    - Run a number of commands
    - End connection
    '''
    count = 1
    while True:
        datetime = subprocess.getoutput('date')
        msg = f' Loop #{count} Time: {datetime}'
        with Connection(MP_IP, MP_UN, connect_kwargs={"password": MP_PW}) as cnx:
            # sudopass = Responder(pattern=r'\[sudo\] password for scking:', response='scking\n')
            delim(title=' System Info ')
            cnx.run('uname -a')
            cnx.run('cat /etc/os-release')
            delim(title=' Networking ')
            cnx.run('/usr/sbin/ip a')
            delim(title=' Storage ')
            cnx.run('lsblk')
            delim(title=msg)
        time.sleep(300)
        count += 1


if __name__ == "__main__":
    main()
