# QA Framework

## Framework
The `framework/` is intended to be installed locally into the python path with:
```
$ pip3 install -e framework/
```
Install from Nexus with:
```
$ pip3 install --index-url https://$NEXUS_USERNAME:$NEXUS_PASSWORD@nexus.dragos.services/repository/pypi-hosted/simple dragos_qa_framework==0.2.0
```


This specific local installation needs to be uninstalled from the python path with:
```
$ pip3 uninstall dragos-qa-framework
```

## Test
This directory contains helper scripts for testing the QA framework code locally by performing:
```
$ .\test\test-local.sh
```
Unit test results, code coverage report and pylint results will be located: `/tmp/test-results`

More details on using this located here: https://dragosinc.atlassian.net/wiki/spaces/EN/pages/1402569832/Running+Framework+Tests


## Jenkinsfile
This is the Jenkinsfile to build and publish the QA framework python package to Nexus.


## Documentation
Complete docs can be found in the `docs/html/` path. Just open `index.html` in your browser of choice.


## How To Build the Sphinx Docs
Sphinx imports and executes Python code to build the documentation. You must have the following environment variables set before starting this procedure (and be connected to the VPN). Please run the QA Framework Installer in the automation_tests repository (https://bitbucket.org/dragosinc/automation_tests/src/master/) to generate an automation_tests_env.sh file before proceeding. Populate this file, source it via `source automation_tests_env.sh`, and return to your `qa_framework/` path.

Alternatively, set the following environment variables manually:
```
export LDAPUSER="yourLDAPusername"
export LDAPPASS='yourLDAPpasword'
export SSHOST="SiteStore FQDN"
export MPHOST="Midpoint FQDN"
export TCPREPLAYHOST="something like traffic-replay.hq.dragos.services"
export REPLAYINTERFACE="something like p4p3"
export UIUSER="yourUIusername"
export UIPASS='yourUIpassword'
```

Once your environment variables are set, proceed.


Clone the repo or branch.
```
git clone git@bitbucket.org:dragosinc/qa_framework.git
```

Build the Docker image from within the Sphinx directory.
```
cd qa_framework/sphinx
./build_docker_image.sh
```

Start Sphinx's Docker container.

IMPORTANT: We want to start the container FROM the qa_framework directory.
```
cd ..
# you should be in <somepath>/qa_framework
sphinx/start_docker_container.sh
```

You should be "in" our Sphinx Docker container now.

NOTE: Your local `qa_automation/` path has been mapped to `/qa_automation/` within the container.
```
cd sphinx/
```

If you are only editing existing modules, you can skip these sphinx-apidoc commands.

If adding new modules, or you are uncertain - run them. They re-build the RST files the HTML is made from.
```
sphinx-apidoc -f -o rst_files/qa_framework/tests/ ../framework/tests/
sphinx-apidoc -f -o rst_files/qa_framework/framework/ ../framework/framework/
```

Clean and make new HTML
```
make clean && make html
```

Nginx is installed in the container, let's serve up the docs!

Start Nginx and copy the HTML files to the web root.
```
nginx
cp -r ../docs/html/* /var/www/html/
```

Open a browser and navigate to:
```
localhost:8080
```
Do you see awesome docs?

Iterate: Work with your docstrings, verify content and style, rinse and repeat...
```
make clean && make html && cp -r ../docs/html/* /var/www/html/
```

Reload the page in your browser.