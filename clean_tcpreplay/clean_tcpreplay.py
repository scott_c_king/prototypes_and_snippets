#!/usr/bin/env python3

'''
Clean SS and MP + TCPReplay PCAP
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"


import json
import subprocess
import time
import sys
from fabric import Connection
from invoke import Responder
from invoke.exceptions import UnexpectedExit

# Root User Shell Prompt:
ROOT_SHELL_PROMPT = r'\[root@{} ~\]# '
RESET_COMMAND = ('(/var/opt/releases/*_reset.sh ; ec=$? ; if [ ${ec} -eq 0 ] ; '
                 'then echo "-- reset exited successfully --" ; fi) '
                 '| tee /home/%s/reset.log && exit\n')


def delim(character='-', length=80, title=''):
    ''' Add a delimiter of chars, length, with optional title. '''
    head = '{:%s^%ss}' % (character, length)
    header = head.format(title)
    print(header)


def import_json(json_file):
    ''' Given a relative/absolute path to JSON file, return Python dictionary. '''
    with open(json_file, 'r') as jfile:
        data = json.loads(jfile.read())
    return data


def get_hostname(connection):
    ''' Fetch the hostname of the remote system '''
    result = connection.run('hostname', hide=True)
    return result.stdout.split('.')[0]


def ping_test(endpoints):
    ''' Pings a list of target FQDN or IPs '''
    try:
        for endpoint in endpoints:
            subprocess.run(f'ping -c 1 {endpoint}', shell=True, check=True)
    except (FileNotFoundError, subprocess.CalledProcessError):
        print(f'ERROR: Could not ping {endpoint}!.\nAre you connected to the VPN?\n'
              'Is local_env.json populated and valid?')
        sys.exit(1)
    print('INFO: Ping tests passed...\n')
    return True


def reset_system(connection, server, username):
    ''' Reset Sitestore(s) and Midpoint(s) '''

    # Fetch the hostname for the root shell prompt
    hostname = get_hostname(connection)
    # Create a readable logfile in user's home directory
    connection.run('touch reset.log && chmod 755 reset.log')

    # Reset the server using the /var/opt/releases reset script
    delim(title=f' Resetting {server} ')
    try:
        reset_cmd = RESET_COMMAND % (username)
        responder = Responder(pattern=ROOT_SHELL_PROMPT.format(hostname), response=reset_cmd)
        begin = time.time()
        connection.run('sudo sudosh', pty=True, watchers=[responder])

    # When exiting sudosh, it will cause an exception - handle it gracefully
    except UnexpectedExit:
        # Ignore the failing exit code from sudosh and check the log file
        cmd = 'cat /home/{}/reset.log'.format(username)
        log_output = connection.run(cmd, hide=True)
        if '-- reset exited successfully --' in log_output.stdout:
            print(f'INFO: {server} reset was successful (Time elapsed: '
                  f'{round((time.time() - begin), 2)} seconds).')
    else:
        print(f'ERROR: {server} reset has failed!')
        sys.exit(1)


def ssh_connection(system, username, password):
    ''' Returns a ssh connection '''
    return Connection(system, username, connect_kwargs={"password": password}, connect_timeout=15)


def copy_pcaps(connection, pcaps, destination):
    ''' Copy a list of pcaps (abs/relative local paths) to target system (/home/username/pcaps) '''
    for pcap in pcaps:
        filename = pcap.split('/')[-1]
        delim(title=f' Copying pcap: {filename} ')
        connection.put(pcap, destination)


def pcap_playback(connection, pcap_path, iface, mbps, username):
    ''' Playback pcaps on target system '''

    # Fetch the hostname for the root shell prompt
    hostname = get_hostname(connection)
    # Create a readable logfile in user's home directory
    connection.run('touch pcap_playback.log && chmod 755 pcap_playback.log')

    filename = pcap_path.split('/')[-1]
    delim(title=f' pcap Playback: {filename} ')
    cmd = ('(tcpreplay-edit -C --mtu-trunc -i %s --stats=30 %s -T nano %s ; ec=$? ; if [ ${ec} -eq 0 ] ; '
           'then echo "--- pcap playback successful ---" ; fi) '
           '| tee /home/%s/pcap_playback.log && exit\n') % (iface, mbps, pcap_path, username)

    try:
        responder = Responder(pattern=ROOT_SHELL_PROMPT.format(hostname), response=cmd)
        begin = time.time()
        connection.run('sudo sudosh', pty=True, watchers=[responder])

    # When exiting sudosh, it will cause an exception - handle it gracefully
    except UnexpectedExit:
        # Ignore the failing exit code from sudosh and check the log file
        cmd = 'cat /home/{}/pcap_playback.log'.format(username)
        log_output = connection.run(cmd, hide=True)
        if '--- pcap playback successful ---' in log_output.stdout:
            print(f'INFO: Playback was successful (Time elapsed: '
                  f'{round((time.time() - begin), 2)} seconds).')
    else:
        print(f'ERROR: {filename} playback has failed!')
        sys.exit(1)


def main():
    ''' Gather some data, iterate over Sitestores and Midpoints, reset each '''

    # Import and assign local_env.json
    data = import_json('local_env.json')
    username = data['ldap_user']
    password = data['ldap_pass']
    sitestores = data['sitestores']
    midpoints = data['midpoints']
    pcaps = data['pcaps']

    # Verify we can ping all of the endpoints
    servers = sitestores + midpoints
    ping_test(servers)

    # Reset Sitestore(s) and Midpoint(s)
    for server in servers:
        with ssh_connection(server, username, password) as cxn:
            reset_system(cxn, server, username)

    # Copy pcaps to target systems
    for midpoint in midpoints:
        with ssh_connection(midpoint, username, password) as cxn:
            dest_path = '/home/{}/pcaps'.format(username)
            cxn.run(f'mkdir -p {dest_path} && chmod -R 755 {dest_path}')
            copy_pcaps(cxn, pcaps, dest_path)

    # Playback pcap(s)
    for midpoint in midpoints:
        print('Midpoint:', midpoint)
        if "cloud" in midpoint:
            interface = 'replay'
        else:
            interface = 'bond0'
        with ssh_connection(midpoint, username, password) as cxn:
            for pcap, speed in pcaps.items():
                pcap_path = '/home/{}/pcaps/{}'.format(username, pcap.split('/')[-1])
                pcap_playback(cxn, pcap_path, interface, speed, username)


if __name__ == "__main__":
    main()
