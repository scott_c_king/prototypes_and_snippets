Clean TCP Replay Utility
------------------------

Clone this repository.
cd clean_tcpreplay
pip install -r requirements.txt
Edit the clean_tcpreplay/local_env file.
RENAME it to local_env.json
python3 clean_tcpreplay.py
Done!


ATTENTION: pcaps Dictionary
---------------------------
key   : absolute or relative path to the pcap
value : the speed of pcap playback

Value parameter:
  empty string = playback at source pcap speed
  "-M 100"     = playback at 100Mbps
  "-M 1000"    = playback at 1Gbps


Examples:
---------
"pcaps"     : {"pcaps/Cyberville_Energy_Center_CTF_OVATION.pcapng" : ""}
Playback the Cyberville_Energy_Center_CTF_OVATION.pcapng at original pcap speed.

"pcaps"     : {"pcaps/FreeportLNG_midpoint01_collectorbond_task_10_pcap-2019-08-16T19_11_05Z.pcap" : "-M 100"}
Playback the FreeportLNG pcap at 100Mbps.

"pcaps"     : {"pcaps/FTE1.pcap" : "-M 1000"}
Playback the FTE1 pcap at 1Gbps.
