How To Build the Sphinx Docs
============================

.. code-block::

   # Clone the repo or branch.
   git clone git@bitbucket.org:dragosinc/qa_framework.git


   # Build the Docker image from within the Sphinx directory.
   cd qa_framework/sphinx
   ./build_docker_image.sh


   # Start Sphinx's Docker container.
   # IMPORTANT: We want to start the container FROM the qa_framework directory.
   cd ..
   sphinx/start_docker_container.sh


   # You should be "in" our Sphinx Docker container now.
   # NOTE: Your local `qa_automation/` path has been mapped to `/qa_automation/` within the container.
   cd sphinx/


   # If you are only editing existing modules, you can skip these sphinx-apidoc commands.
   # If adding new modules, or you are uncertain - run them. They re-build the RST files the HTML is made from.
   sphinx-apidoc -f -o rst_files/qa_framework/tests/ ../framework/tests/
   sphinx-apidoc -f -o rst_files/qa_framework/framework/ ../framework/framework/


   # Clean and make new HTML
   make clean && make html


   # Want to see what the docs will look like?
   # Start Nginx and copy the HTML files to the web root
   nginx
   cp -r ../docs/html/* /var/www/html/


   # Open a browser and navigate to:
   localhost:8080


   # Iterate: Verify your grammar and style, rinse and repeat...
   make clean && make html && cp -r ../docs/html/* /var/www/html/


   # Reload the page in your browser.

